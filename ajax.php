<?php

require("functions.php");

$action = $_REQUEST['action'];

switch ($action) {
	case "DELETE":
		delete_files();
		break;
	case "PROCESS_FILE":
		process_uploaded_file($_SERVER["DOCUMENT_ROOT"] . "/upload/" . strip_tags($_REQUEST['file']));
		break;
}
?>