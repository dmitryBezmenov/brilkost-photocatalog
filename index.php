<?

require_once("functions.php");
require_once("navigation.php");
require_once("result.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/_include/header.php");

$rows = Result::get();

?>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<div class="catalog">

	<?if (!empty($rows)):?>

        <form action="download.php" class="form-download">
            <div class="container">
                <div class="catalog__count">Найдено: <?=count($rows)?></div>
                <div class="row">
                    <? foreach ($rows as $key => $row): ?>
						<?if (empty($row['FILES'])) continue;?>
						<div class="col-2 col-xxl-2">
							<div class="card">
								<div class="card__inner">
									<label for="card-<?= $key ?>" class="card-checkbox">
										<input type="checkbox" name="card[]" id="card-<?= $key ?>"
											class="card-checkbox__input" value="<?= $key ?>">
										<span class="card-checkbox__icon"></span>
									</label>
									<div class="card__head">
										<div class="card__thumbnail">
											<img src="image.php?img=<?=array_values($row['FILES'])[0]?>" alt="" class="card__image">
										</div>
									</div>
									<div class="card__title"><?= $row["ART"] ?></div>
								</div>
								<div class="card__onhover">
									<div class="card__buttons">
										<?foreach ($row['FILES'] as $file):?>
											<a href="image.php?img=<?=$file?>" class="button card__button"><?=pathinfo($file, PATHINFO_EXTENSION)?></a>
										<?endforeach;?>
									</div>
								</div>
							</div>
						</div>
					<? endforeach; ?>
                </div>
            </div>

            <div class="catalog-fixed">
					<?if (false):?>
						<div class="catalog-fixed__container container">
							<div class="b-pagination catalog__pagination">
								<?
									$navi = new PaginateNavigationBuilder( "/manager" . ($filter ? "?flt=" . $filter : ""));
									$navi->tpl = ($filter ? '&p={page}'  : '?p={page}');
									$navi->wrap = '<ul class="b-pagination__list">{pages}</ul>';
									$navi->item_wrap = '<li class="b-pagination__item"><a href="{page_link}" class="b-pagination__link {cur}" >{page}</a></li>';
									$navi->separator = '<li class="b-pagination__item">...</li>';
									$navi->spread = 3;
									$template = $navi->build( $PAGE_SIZE, $total, $page + 1);
									echo($template);
								?>							
							</div>
						</div>
					<? endif ?>
                    <div class="catalog-panel">
						<div class="catalog-panel__container container">
							<div class="form-download__controls">
								<div class="form-download__left">
									<label for="" class="label">Выбрано <span class="selected-items">0</span> изделий</label>
									<button type="button" class="form-download__unselect d-none">
										<svg class="icon-svg icon-svg--close">
											<use xlink:href="#icon-close"></use>
										</svg>
										отменить выбор
									</button>
								</div>
								<div class="form-download__right">
									<div class="form-download__format">
										<label for="" class="label">Формат файлов</label>
										<div class="format-radio-group">
											<label for="fr2-1" class="format-radio">
												<input type="radio" name="format" id="fr2-1" class="format-radio__input" value="all" checked>
												<span class="format-radio__button">
											Все
										</span>
											</label>
											<label for="fr2-2" class="format-radio">
												<input type="radio" name="format" id="fr2-2" class="format-radio__input" value="jpeg">
												<span class="format-radio__button">
											Jpg
										</span>
											</label>
											<label for="fr2-3" class="format-radio">
												<input type="radio" name="format" id="fr2-3" class="format-radio__input" value="tif">
												<span class="format-radio__button">
											Tiff
										</span>
											</label>
										</div>
									</div>
									<button type="submit" class="button form-download__submit">
										Скачать выбранные изделия (<span class="selected-items">0</span>)
									</button>
								</div>
							</div>
						</div>
                    </div>
                
            </div>
        </form>
    </div>

	<?endif;?>

	 <div class="upload-text d-none">
        <div class="upload-text__content">
            Загрузите XLS файл или воспользуйтесь поиском, чтобы начать работу
        </div>
    </div>

<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/_include/footer.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/_include/for_all_pages.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/_include/modals.php");
?>