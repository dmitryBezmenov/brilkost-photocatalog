function getGetParam() {
    var tmp = new Array();
    var tmp2 = new Array();
    var param = new Array();
    var get = location.search;
    if (get != '') {
        tmp = (get.substr(1)).split('&');
        len = tmp.length;
        for (var i = 0; i < len; i++) {
            tmp2 = tmp[i].split('=');
            param[tmp2[0]] = tmp2[1];
        }
        return param;
    }
    return false;
}

function getRandomArbitary(min, max)
{
    return Math.random() * (max - min) + min;
}

//блюр и анблюр
function blur(rly) {
    if (rly) {
        $('.pageHeader, .container, .pageFooter').addClass('blur');
    } else {
        $('.pageHeader, .container, .pageFooter').removeClass('blur');
    }
}


// красивый сбор данных с форм
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function () {
    // forInt - класс для инпут-текст, в которые можно вводить только число
    $('body').on('change keyup', '.forInt', function () {
        $(this).val(parseInt($(this).val()) | 0);
        if ($(this).val() < 1) {
            $(this).val(1);
        }
    });

    //замена кавычек в input
    $('input[type="text"]').keyup(function() {
        $(this).each (function () {
            var replaced = $(this).val().replace(/"/g, "'");
            $(this).val(replaced);
        });
    });

    // включаем стайлер для всех селектов
    $('select:not(".nostyler"), input[type=checkbox]:not(".nostyler")').styler({
        selectVisibleOptions: 5,
        selectSmartPositioning: false
    });

    // fancybox
    $('.fancybox').fancybox({
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0,0,0,.2)'
                }
            }
        },
        beforeShow: function () {
            blur(true);
        },
        afterClose: function () {
            blur(false);
        }
    });

    //работа табов
    $(".tabs .tabButton").click(function () {
        $('.tabsPages>div.mediaElementWrapper').hide();
        $('.tabsPages>div.mediaElementWrapper').eq($(this).index()).show();
        $('.tabs .tabButton').removeClass('active');
        $(this).addClass('active');
    });

    // лог аут
    $('.logout').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {type: 'logout'},
            success: function () {
                location.reload(true);
            }
        });
    });

    // модал для входа
    var enterModal = new jBox('Modal', {
        closeOnClick: 'overlay',
        closeButton: 'box',
        addClass: 'loginForm',
        onOpen: function () {
            blur(true);
        },
        onClose: function () {
            blur(false);
        },
        ajax: {
            type: "POST",
            url: '/ajax.php',
            data: {
                type: 'logOnModal',
                data: {
                    "TYPE": 'modal'
                }
            },
            reload: true
        }
    });



    // лог он
    $('.enter').click(function (e) {
        enterModal.open();
    });



    $('.cancel').click(function (e) {
        // enterCancel.open();
        modalCancel.open();
    });
    var modalCancel = new jBox('Modal', {
        closeOnClick: 'overlay',
        closeButton: 'box',
        addClass: 'cancelForm',
        onOpen: function () {
            blur(true);
            window.setTimeout('$("input[name=numberPhone]").inputmask("+7(999)999-99-99")', 500);
        },
        onClose: function () {
            blur(false);
        },
        ajax: {
            type: "POST",
            url: '/ajax.php',
            data: {
                type: 'cancelModal',
                data: {
                    "id": $('.cancel').attr('data-order')
                }
            },
            reload: false
        }
    });

    $("body").on("click", '.confirmCancel',function (event) {
        event.preventDefault();
        $('.modal-cancel').css('display','none');
        $('.jBox-closeButton').css('display','none');
        $('.modal-cancel-done').fadeIn();

        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {
                type: 'cancelOrder',
                data: {
                    "ID": id
                }
            }
        }).done(function () {
            location.href = location.href;
        });
    });



    $('body')



    $('body').on('submit', '.logOnForm', function (event) {
        event.preventDefault();
        var info = $(this).serializeObject();
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {
                type: 'logOnModal',
                data: {
                    "TYPE": 'logon',
                    "LOGON_DATA": info
                }
            },
            success: function (msg) {
                var asnwer = jQuery.parseJSON(msg);
                if (asnwer.type === 'ok') {
                    location.href = '/catalog/';
                } else if (asnwer.type === 'error') {
                    $('.logOnForm input[type=text], input[type=password]').addClass('error')
                        .click(function () {
                            $('.logOnForm input[type=text], .loginForm input[type=password]').removeClass('error');
                            $('.logOnForm .errorText').fadeOut();
                        });
                    $('.logOnForm .errorText').fadeIn().find('div').html(asnwer.message);
                }
            }
        });
    });

    //модал для перезвона
    var enterRecall = new jBox('Modal', {
        closeOnClick: 'overlay',
        closeButton: 'box',
        addClass: 'recallForm',
        onOpen: function () {
            blur(true);
            window.setTimeout('$("input[name=numberPhone]").inputmask("+7(999)999-99-99")', 500);
        },
        onClose: function () {
            blur(false);
        },
        ajax: {
            type: "POST",
            url: '/ajax.php',
            data: {
                type: 'recallModal'
            },
            reload: false
        },
        onCreated: function () {
            //$("input[name=numberPhone]").inputmask("+7(999)999-99-99");
            //window.setTimeout('$("input[name=numberPhone]").inputmask("+7(999)999-99-99")', 1000);
        }
    });

    $('.recall').click(function (e) {
        enterRecall.open();
    });
    $('body').on('submit', '.recallForm', function (event) {
        event.preventDefault();
        var err = false;
        var inputName = $('.recallForm input[name=name]');
        var inputPhone = $('.recallForm input[name=numberPhone]');
        console.log(inputPhone.val());
        console.log(inputPhone.val().indexOf('_'));
        if (inputName.val() == "") {
            err = true;
            inputName.addClass("error");
        }
        if (inputPhone.val() == "") {
            err = true;
            inputPhone.addClass("error");
        } else if(inputPhone.val().indexOf('_') !== -1) {
            err = true;
            inputPhone.addClass("error");
        }
        if (!err) {
            var form_data = $(this).serializeObject();
            form_data.type = 'recallMail';
            $.ajax({
                type: "POST",
                url: "/ajax.php",
                data: form_data,
                success: function (msg) {
                    $('.recallForm .jBox-content').html('<div class="commonText">В ближайшее время наш менеджер свяжется с Вами</div>')
                }
            });
        }
        return false;
    });
    $('body').on('click', 'input.error', function () {
        $(this).removeClass('error')
    });

    // забыли пароль
    $('body').on('click', '.forgorPassword', function () {
        $('.loginForm .title').html('Восстановление пароля');
        $('.loginForm .logOnForm').hide();
        $('.loginForm .forgotPasswordForm').show();
    });
    $('body').on('submit', '.forgotPasswordForm', function (event) {
        event.preventDefault();
        var info = $(this).serializeObject();
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: {
                type: 'logOnModal',
                data: {
                    "TYPE": 'forgotPassword',
                    "LOGON_DATA": info
                }
            },
            success: function (msg) {
                var asnwer = jQuery.parseJSON(msg);
                if (asnwer.type === 'ok') {
                    $('.forgotPasswordForm').html('<div class="commonText">Новый пароль был выслан вам на электронную почту</div>')
                    $('.loginForm').find('hr, a').remove();
                } else if (asnwer.type === 'error') {
                    $('.forgotPasswordForm input[type=text]').addClass('error')
                        .click(function () {
                            $('.forgotPasswordForm input[type=text]').removeClass('error');
                            $('.loginForm .errorTextForgotPassword').fadeOut();
                        });
                    $('.forgotPasswordForm .errorTextForgotPassword').fadeIn().find('div').html(asnwer.message);
                }
            }
        });
    });

    // обновление количества товара в корзине
    $('.c_count').on('change',function (e) {
        e.preventDefault();
        var quan = $(this).val();

        if (quan > 999 || quan < 1){
            $(this).val('1');
            return;
        }

        var id = $(this).attr('data-id');
        data = {
            type: 'updateQuantity',
            quan: quan,
            id: id
        };
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: data,
            success: function (msg) {
                if (msg > 0) {
                    console.log('OK');
                } else {
                    console.log('Произошла ошибка');
                }
            }
        });
    });

    // обновление металла в корзине
    $('.c_metal').on('change',function (e) {
        e.preventDefault();
        var metal = $(this).val();
        var id = $(this).attr('data-id');
        data = {
            type: 'updateMetal',
            metal:metal,
            id: id
        };
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: data,
            success: function (msg) {
                if (msg > 0) {
                    console.log('OK');
                } else {
                    console.log('Произошла ошибка');
                }
            }
        });
    });

    // обновление вставки товара в корзине
    $('.c_insert').on('change',function (e) {
        e.preventDefault();
        var insert = $(this).val();
        var id = $(this).attr('data-id');
        // console.log(metal,id);
        data = {
            type: 'updateInsert',
            insert: insert,
            id: id
        };
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: data,
            success: function (msg) {
                if (msg > 0) {
                    console.log('ОК');
                } else {
                    console.log('Произошла ошибка');
                }
            }
        });
    });
    //обновление размера изделия в корзине
    $('.c_size').on('change',function (e) {
        e.preventDefault();
        var size = $(this).val();
        var id = $(this).attr('data-id');
        // console.log(size,id);
        data = {
            type: 'updateSize',
            size: size,
            id: id
        };
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: data,
            success: function (msg) {
                if (msg > 0) {
                    console.log('ОК');
                } else {
                    console.log('Произошла ошибка');
                }
            }
        });
    });


    // добавление в корзину
    $('.addToCart').on('click', function (e) {
        e.preventDefault();
        var size = $('#p_size').val();
        if (size === undefined) {
            size = 0;
        }
        data = {
            type: 'addToCart',
            art_product: $(this).attr('data-art'),
            art_offer: $('#p_insert option:selected').attr('data-id'),
            size: size,
            metal: $('#p_metals').val(),
            insert: $('#p_insert').val(),
            count: $('#p_count').val(),
            prod_type: $(this).attr('data-prod-type')
        };
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: data,
            success: function (msg) {
                if (msg > 0) {
                    addToCartModal.open();
                } else {
                    console.log('Произошла ошибка');
                }
            }
        });
    })

    $('.newSlider').owlCarousel({
        items: 1,
        dots: true,
        loop: true,
        nav: false,
        autoplay: true,
        autoplayTimeout: 4000
    });
    $('.detailSlider').owlCarousel({
        items: 1,
        dots: true,
        loop: true,
        nav: false,
        autoplay: true,
        autoplayTimeout: 4000,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
    });
    // слайдеры паззла
    $('.demoSlider').each(function(i, el){
        $(el).slick({
            dots: false,
            infinite: true,
            speed: 1000,
            fade: true,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: getRandomArbitary(3000, 15000),
            arrows: false
        });
    });
    // слайдеры открытого каталога
    $('.mainPuzzleElement').each(function(i, el){
        $(el).slick({
            dots: false,
            infinite: true,
            speed: 1000,
            fade: true,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: getRandomArbitary(7000, 20000),
            arrows: false
        });
    });

});


window.onload = function() {
    $('.photoElement').add('.smallImgElem').css('margin-right', 0);
    $('.photoElementWrapper').masonry({
        itemSelector: '.photoElement',
        columnWidth: 230,
        gutter: 19
    });
    $('.smallImg').masonry({
        itemSelector: '.smallImgElem',
        columnWidth: 230,
        gutter: 19
    });
};





