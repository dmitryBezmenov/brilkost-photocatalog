var myMap, myPlacemark;
ymaps.ready(init);

function init () {
    myMap = new ymaps.Map('map', {
        center: [57.7678, 40.9674],
        zoom: 13,
        controls: [ ]
    });
    myMap.behaviors.disable("scrollZoom");
    myMap.controls.add('zoomControl', {position: {left: '10px', top: '10px'}});
    57.76774898, 40.96747349
    myPlacemark = new ymaps.Placemark([57.75743971, 40.98685984], {
        //hintContent: 'Собственный значок метки'
        balloonContentHeader: "Брилианты Костромы",
        balloonContentBody: "156019, г. Кострома, ул. Локомотивная, д. 2 ж<br/ > +7 (4942) 42-20-23, факс: 42-20-32<br />Сайт: <nobr><a href=\"www.bril-kost.ru\">www.bril-kost.ru</a></nobr><br />Эл. почта: <nobr><a href=\"mailto:info@bril-kost.ru?subject=Письмо с сайта Бриллианты Костромы\">info@bril-kost.ru</a></nobr>",
        balloonContentFooter: "Время работы<br /> 9:00&nbsp;&mdash; 18:00"
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/_img/iconCon.png',
        // Размеры метки.
        iconImageSize: [56, 70],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-28, -70]
    });

    myMap.geoObjects.add(myPlacemark);

}