$(".bibl_image_container").hover(
	function(){
		obj = $(this).children(".bibl_img");
		obj.width(obj.width()+30);
		obj.css("left", "-15px");
		obj.height(obj.height()+30);
		obj.css("top", "-15px");
		obj.css("box-shadow", "0 0 6px 0 gray");
	},
	function(){
		obj = $(this).children(".bibl_img");
		obj.width("100%");
		obj.css("left", "0px");
		obj.height("100%");
		obj.css("top", "0px");
		obj.css("box-shadow", "0 0 0 0 gray");
	}
);

$(".enc_hover").hover(
	function(){
		obj = $(this).children(".enc_col").children(".enc_col_bg");
		obj.width(obj.width()+26);
		obj.css("left", "-13px");
		obj.height(obj.height()+40);
		obj.css("top", "-20px");
		obj.css("box-shadow", "0 0 8px 0 gray");
	},
	function(){
		obj = $(this).children(".enc_col").children(".enc_col_bg");
		obj.width("100%");
		obj.css("left", "0px");
		obj.height("100%");
		obj.css("top", "0px");
		obj.css("box-shadow", "0 0 0 0 gray");
	}
);
