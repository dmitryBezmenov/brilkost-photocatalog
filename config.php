<?php

class Config
{
    static protected $data = 
    [
        "ZIP_NAME" => "images.zip",
        "INCLUDE_ART_WITH_NO_INS" => false,
        "PHOTO_PATH" => "D:\\OpenServer\\domains\\dev-photokatalog\\_img\\jewelry\\",
        "PHOTO_PATH2" => "",
        "STORE_PATH" => "/upload/",
        "PAGE_SIZE" => 2000,
        "PROGRESS_FILE_NAME" => "process.txt",
    ];

    public static function get(string $name)
    {
        if (!isset(static::$data[$name]))
            throw new \Exception("try again pls");

        return static::$data[$name];
    }
}