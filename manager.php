<?php

class FilesListManager
{
    protected $listPath;
    protected $filesPath;
    protected $updatedAt = 0;
    protected $list = [];

    public function __construct(string $listPath, string $filesPath)
    {
        $this->listPath = $listPath;
        $this->filesPath = $filesPath;
    }

    public function getList()
    {
        return $this->list;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function update()
    {   
        $this->list = getDirContents($this->filesPath);
        $this->updatedAt = time();

        $this->save();
    }

    public function save()
    {
        file_put_contents($this->listPath, serialize([
            "list" => $this->list,
            "updated_at" => $this->updatedAt
        ]));
    }

    public function loadList()
    {
        if (!file_exists($this->listPath))
            return false;

        $data = unserialize(file_get_contents($this->listPath));

        $this->updatedAt = $data["updated_at"];
        $this->list = $data["list"];

        return true;
    }
}


function getDirContents($dir, &$results = array()) 
{
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != "." && $value != "..") {
            getDirContents($path, $results);
        }
    }

    return $results;
}