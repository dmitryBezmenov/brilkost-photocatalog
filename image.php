<?php

$img = $_GET["img"];

$mimeTypes = array(
	"jpg" => "image/jpeg",
	"jpeg" => "image/jpeg",
	"tif" => "image/tiff",
	"tiff" => "image/tiff",
);

$mimeType = $mimeTypes[pathinfo($img, PATHINFO_EXTENSION)];

if (!file_exists($img))
	exit();


if (ob_get_level()) {
	ob_end_clean();
}
header('Content-Type: ' . $mimeType);
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($img));
readfile($img);
exit;