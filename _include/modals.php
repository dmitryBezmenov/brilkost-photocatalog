<!--  -->
<div class="modal fade" id="file-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                Esc
                <svg class="icon-svg icon-svg--close">
                    <use xlink:href="#icon-close"></use>
                </svg>
            </button>
            <div class="modal-title">Загрузка xls файла</div>
            <div class="modal-body">
                <form action="" class="form-upload">
                    <!--<div class="file">
                        <div class="file__dnd js-file-dnd">

                        </div>
                        <div class="file__progress js-file-progress">

                        </div>
                    </div>-->
                    <div class="file">
                        <div class="file-dragndrop js-dropzone">
                            <div class="file-dragndrop__container">
                                <div class="file-dragndrop__inner">
                                    Перетащите файл в эту область или <span>выберите файл на компьютере</span>
                                </div>
                            </div>
                        </div>
                        <div class="file-preview js-dropzone-previews"></div>
                        <!--<div id="file-preview">
                            <div class="dz-preview dz-file-preview well" id="dz-preview-template">
                                <div class="dz-details">
                                    <div class="dz-filename"><span data-dz-name></span></div>
                                    <div class="dz-size" data-dz-size></div>
                                </div>
                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span>
                                    <div class="dz-percentage" data-dz-percentage></div>
                                </div>
                                <div class="dz-success-mark"><span></span></div>
                                <div class="dz-error-mark"><span></span></div>
                                <div class="dz-error-message"><span data-dz-errormessage></span></div>
                            </div>
                        </div>-->
                    </div>
                    <button type="submit" class="button form-upload__submit" disabled>Загрузить</button>
                </form>
            </div>
        </div>
    </div>
</div>