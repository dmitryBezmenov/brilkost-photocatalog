 <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="/_css/vendor.min.css" type="text/css" rel="stylesheet"/>
<link href="/_css/styles-photo.css" type="text/css" rel="stylesheet"/>

<script src="/_js/vendor.min.js"></script>