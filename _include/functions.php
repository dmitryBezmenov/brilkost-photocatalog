<?php

/**
 * svg - инклудит минифицированный svg
 * @param string $p путь к svg относительно корня сайта
 * @return string
 */
function svg($p) {
    $p = $_SERVER['DOCUMENT_ROOT'] . $p;
    return file_exists($p) ? preg_replace('/<[?|!].*?>/', '', str_replace(array("\r\n", "\r", "\n", "\t", '  '), '', file_get_contents($p))) : '';
}
