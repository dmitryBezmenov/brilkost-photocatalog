<?
$filter = $_REQUEST["flt"] ? htmlspecialchars($_REQUEST["flt"]) : false;

$uploadPage = ["/upload.php", "/manager/upload.php"];
?>

<title>Фотокаталог</title>

<div class="layout">
    <header class="header">
        <div class="header__container container">
            <div class="header__left">
                <a href="/" class="header__logo">
                    <img src="/app/img/svg/logo.svg" alt="">
                </a>
				
                <div class="form-search header__search">
                    <div class="form-search__container">
                        <div class="form-search__inner">
							<!-- <form>
                            <input type="text" name="flt" placeholder="Поиск по артикулу"
                                   class="input form-search__input" <?//=($filter ? 'value="' . $filter . '"' : '')?>>
                            <button class="form-search__submit" type="submit">
                                <svg class="icon-svg icon-svg--loop">
                                    <use xlink:href="#icon-loop"></use>
                                </svg>
                            </button>
							</form> -->
                        </div>
                        <div class="form-search__suggest">
                            <div class="form-search__scroll">
                                <? for ($i = 0; $i < 15; $i++): ?>
                                    <a href="" class="form-search__item">
                                        <div class="form-search__thumbnail">
                                            <img src="/app/img/card-1.png" alt="01-1262/73ПР-98">
                                        </div>
                                        <div class="form-search__info">
                                            <div class="form-search__name"><span></span></div>
                                        </div>
                                    </a>
                                <? endfor; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__right">
                
                    <!-- Загрузка -->
                    <div class="upload">
                        <button type="button" class="header__upload" data-toggle="modal" data-target="#file-upload">
                            <svg class="icon-svg icon-svg--upload">
                                <use xlink:href="#icon-upload"></use>
                            </svg>
                            Загрузить XLS файл
                        </button>
                    </div>
                
                    <form action="download.php" class="form-download header__download hidden">
                        <div class="form-download__controls">
                            <!-- <div class="form-download__file">
                                <div class="form-download__file-wrap">
                                    <div class="form-download__file-overflow">
                                        Таблица 1 Таблица 2 3 Таблица 2 3.xls
                                    </div>
                                    <button type="button" class="form-download__delete">Удалить</button>
                                </div>
                            </div>
                            <div class="form-download__separate"></div>
                            <div class="form-download__format">
                                <label for="" class="label">Формат файлов</label>
                                <div class="format-radio-group">
                                    <label for="fr-1" class="format-radio">
                                        <input type="radio" name="format" id="fr-1" class="format-radio__input" value="all" checked>
                                        <span class="format-radio__button">
                                        Все
                                    </span>
                                    </label>
                                    <label for="fr-2" class="format-radio">
                                        <input type="radio" name="format" id="fr-2" value="jpeg" class="format-radio__input">
                                        <span class="format-radio__button">
                                        Jpg
                                    </span>
                                    </label>
                                    <label for="fr-3" class="format-radio">
                                        <input type="radio" name="format" id="fr-3" value="tif" class="format-radio__input">
                                        <span class="format-radio__button">
                                        Tiff
                                    </span>
                                    </label>
                                </div>
                            </div> -->
                            <button type="submit" class="button form-download__submit download-all">
                                Скачать все изделия
                            </button>
                        </div>
                    </form>
                

            </div>
        </div>
    </header>
    <div class="layout__content">
